import React, { Component } from "react";
import axios from "axios";
import HeroesMarvelList from "./components/HeroesMarvelList";
import chave from "./apiKey.js";

class App extends Component {

	constructor(props) {
		super(props);
		this.state = {
			listHeroes: [],
			character: '',
			orderBy: '',
			page: 1,
			pages: 0,
			key: ''
		};
		
		this.handleChangeInput  = this.handleChangeInput.bind(this);
		this.handleChangeSelect = this.handleChangeSelect.bind(this);
		this.handleChangePage   = this.handleChangePage.bind(this);
	}
	
	componentDidMount() {
		/* variável para retorno de valores para filtro */
		var filterVar = "";
		/* query string */
		let url = window.location.search.substr(1);
		/* verifica se tem query string */
		if(url !== "" && url !== undefined){
			let filtro = url.split("&");

			/* checagem de campos separados */
			for(let i=0; i<filtro.length;i++){
				let valores = filtro[i].split("=");

				/* se tem valor preenchido, verifica campo e cria filtro pra API */
				if(valores[1] !== ""){
					switch(valores[0]) {
						case 'character':
							valores[0] = "nameStartsWith";
							this.setState({character: valores[1]});
							break;
						case 'orderBy':
							this.setState({orderBy: valores[1]});
							break;
						case 'page':
							valores[0] = "offset";

							this.setState({page: valores[1]});
							break;
					}
					
					/* após checagem do valor, cria o filtro pra API */
					let valorFinal = valores.join('=');
					filterVar += "&"+valorFinal;
				}
			}
			//console.log(filterVar);
		}

		const apiLink = "https://gateway.marvel.com:443/v1/public/";
		let list = "characters?apikey="+chave+"&limit=12"
		
		axios
			.get(apiLink+list+filterVar)
			.then(response => {
				let pages    = Math.ceil(response.data.data.total / 12);
				//console.log("Total Página ",pages);
				let results  = response.data.data.results;
				
				const heroes = results.map(h => {
					//console.log(h);
					let image = `${h.thumbnail.path}.${h.thumbnail.extension}`;
					let nameSplit = h.name.split("(");
					let name = nameSplit[0];
					if(nameSplit[1] !== undefined){
						var realName = nameSplit[1].substr(0,(nameSplit[1].length - 1));
					}

					return {
						id: h.id,
						link: h.resourceURI,
						image: image,
						name: name,
						realName: realName,
						description: h.description
					};
				});

				this.setState({listHeroes: heroes});
				this.setState({pages: pages});
			})
			.catch(error => console.log(error));
	}

	handleChangeInput(event) {
		this.setState({character: event.target.value});
		this.setState({page: 1});
	}

	handleChangeSelect(event) {
		this.setState({orderBy: event.target.value});
	}

	handleChangePage(event) {
		this.setState({page: event.target.value});
	}

	render() {
		//console.log("paginacao ", this.state.pages);
		
		var paginacao = [];
		for (var i = 1; i <= this.state.pages; i++) {
			paginacao.push(<option key={i} value={i}>{i}</option>);
		}

		return (
			<div className="App">
				<header>
					<h1>Marvel Comics</h1>
				</header>
				<main>
					<div>
						<h2>Characters</h2>

						<form action="" method="get">
							<input type="text" name="character" placeholder="Nome do Personagem" value={this.state.character} onChange={this.handleChangeInput} />
							
							<select name="orderBy" value={this.state.orderBy} onChange={this.handleChangeSelect}>
								<option value="name">A-Z</option>
								<option value="-name">Z-A</option>
							</select>

							<select name="page" value={this.state.page} onChange={this.handleChangePage}>
								{paginacao}
							</select>
							
							<input type="submit" value="Procurar" />
						</form>

						<HeroesMarvelList listHeroes={this.state.listHeroes} />
					</div>
				</main>
				<footer>
					<div>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
					</div>
				</footer>
			</div>
		);
	}
}

export default App;
