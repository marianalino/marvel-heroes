import React from "react";
import PropTypes from "prop-types";

// import the Contact component
import HeroesMarvel from "./HeroesMarvel";

function HeroesMarvelList(props) {
	return (
		<ul id="listCharacters">
			{props.listHeroes.map(h =>
				<HeroesMarvel
					key={h.id}
					id={h.id}
					link={h.link}
					image={h.image}
					name={h.name}
					realName={h.realName}
					description={h.description}
				/>
			)}
		</ul>
	);
}

HeroesMarvelList.propTypes = {
	listHeroes: PropTypes.array.isRequired
};

export default HeroesMarvelList;
