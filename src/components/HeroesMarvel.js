import React from "react";
import PropTypes from "prop-types";

function HeroesMarvel(props) {
	return (
		<li key={props.id}>
			<img src={props.image} alt={props.name} />
			<h3>{props.id} - {props.name}</h3>
			<h4>{props.realName}</h4>
			<hr />
			<p>{props.description}</p>
		</li>
	);
}

HeroesMarvel.propTypes = {
	name: PropTypes.string.isRequired
};

export default HeroesMarvel;
