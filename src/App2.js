import React from "react";
import axios from "axios";
import HeroesMarvelList from "./components/HeroesMarvelList";
import chave from "./apiKey.js";
var createReactClass = require('create-react-class');

//class App extends Component {

var App = createReactClass({	

	filterList(event){
		var updatedList = this.state.heroes;
		updatedList = updatedList.filter(function(item){
			return item.toLowerCase().search(
				event.target.value.toLowerCase()
			) !== -1;
		});
		this.setState({listHeroes: updatedList});
	},
	getInitialState(){
		//console.log('entrou');
		
		axios
			.get("https://gateway.marvel.com:443/v1/public/characters?apikey="+chave)
			.then(response => {
				let results = response.data.data.results;
				console.log("1 ",typeof results);
				//
				return {
					heroes: results,
					listHeroes: []
				};
				this.setState({listHeroes: results});
				//results.map(h => {
				//	//console.log(h);
				//	let image = `${h.thumbnail.path}.${h.thumbnail.extension}`;

				//});
			})
			.catch(error => console.log(error));
	
		console.log("1 ",typeof results);
		return {
			heroes: [ "teste", "teste 1"],
			listHeroes: []
		}
		
	
	},
	componentWillMount(){
		console.log(typeof this.state.heroes);
		console.log(typeof this.state.listHeroes);
		this.setState({listHeroes: this.state.heroes})
	},
	render() {
		return (
			<div className="App">
				<header>
					<h1>Marvel Comics</h1>
				</header>
				<main>
					<div>
						<h2>Characters</h2>

						<form onSubmit={this.onFormSubmit} action="http://localhost:3000" method="post">
							<input type="text" placeholder="Search" onChange={this.filterList}/>
						</form>

						<HeroesMarvelList listHeroes={this.state.listHeroes} />
					</div>
				</main>
				<footer>
					<div>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
					</div>
				</footer>
			</div>
		);
	}

});

/*var HeroesMarvelList = createReactClass({
  render: function(){
	return (
	  <ul>
	  {
		this.props.listHeroes.map(function(item) {
		  return <li key={item}>{item}</li>
		})
	   }
	  </ul>
	)  
  }
});*/


//}

export default App;
